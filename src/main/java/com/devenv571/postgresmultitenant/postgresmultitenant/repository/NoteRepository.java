package com.devenv571.postgresmultitenant.postgresmultitenant.repository;

import com.devenv571.postgresmultitenant.postgresmultitenant.data.Note;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoteRepository extends CrudRepository<Note, Long> {
}
