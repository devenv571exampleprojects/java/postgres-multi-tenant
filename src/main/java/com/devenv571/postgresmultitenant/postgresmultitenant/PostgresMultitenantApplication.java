package com.devenv571.postgresmultitenant.postgresmultitenant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostgresMultitenantApplication {

    public static void main(String[] args) {
        SpringApplication.run(PostgresMultitenantApplication.class, args);
    }

}
