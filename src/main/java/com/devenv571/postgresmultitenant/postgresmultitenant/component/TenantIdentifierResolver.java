package com.devenv571.postgresmultitenant.postgresmultitenant.component;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class TenantIdentifierResolver implements CurrentTenantIdentifierResolver {

    public static final String DEFAULT_TENANT = "public";

    @Override
    public String resolveCurrentTenantIdentifier() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            return DEFAULT_TENANT;
        }

        Object principal = auth.getPrincipal();
        if (principal instanceof KeycloakPrincipal) {
            KeycloakSecurityContext keycloakSecurityContext = ((KeycloakPrincipal) principal).getKeycloakSecurityContext();
            return "APP_" + keycloakSecurityContext.getToken().getPreferredUsername();
        }

        return DEFAULT_TENANT;
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }
}
