package com.devenv571.postgresmultitenant.postgresmultitenant.service;

import com.devenv571.postgresmultitenant.postgresmultitenant.data.Note;
import com.devenv571.postgresmultitenant.postgresmultitenant.repository.NoteRepository;
import org.springframework.stereotype.Service;

@Service
public class NoteService {

    private final NoteRepository repository;

    public NoteService(NoteRepository repository) {
        this.repository = repository;
    }

    public Note createNote(Note note) {
        return repository.save(note);
    }

    public Note findNote(Long id) {
        return repository.findById(id).orElseThrow();
    }

    public Iterable<Note> findAllNotes() {
        return repository.findAll();
    }
}
