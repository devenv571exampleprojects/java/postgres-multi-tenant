package com.devenv571.postgresmultitenant.postgresmultitenant.controller;

import org.flywaydb.core.Flyway;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;

@RestController
@RequestMapping("tenant")
public class TenantController {

    private final DataSource dataSource;

    public TenantController(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @PostMapping("{tenant}")
    public void createTenant(@PathVariable("tenant") String tenant) {
        Flyway flyway = Flyway.configure()
                .locations("db/migration/tenants")
                .dataSource(dataSource)
                .schemas("APP_" + tenant)
                .load();
        flyway.migrate();
    }
}
