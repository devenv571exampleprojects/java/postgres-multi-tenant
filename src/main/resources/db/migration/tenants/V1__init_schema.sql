CREATE SEQUENCE note_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE note_id_seq
    OWNER TO postgres;

CREATE TABLE note
(
    id bigint NOT NULL DEFAULT nextval('note_id_seq'::regclass),
    text character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT note_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE note
    OWNER to postgres;